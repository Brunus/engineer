# L'Ingénieur - classe de personnage pour Donjons & Dragons

L'Ingénieur est une sous-classe de magicien qui associe magie et fabrication d'objets et/ou de mécanismes, de machines.
Un peu noter par exemple la fabrication d'armes à mécanismes, de pièges, de golems, d'exosquelettes, de machines volantes etc.
Léonard De Vinci peut être un personnage historique utilisé comme exemple d'Ingénieur bien qu'ils n'utilisait pas la magie.
